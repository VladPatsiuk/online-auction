var config = require('config.json');
var express = require('express');
var router = express.Router();
var auctionService = require('services/auction.service');

// routes
router.get('/', getAll);
router.get('/:name', getByLotName);
router.get('/:type', getByLotType);
router.get('/:_id', getById);
router.post('/create_auction', createAuction)
router.post('/add_bid', addBid);
router.put('/:_id', update);
router.delete('/:_id', _delete);

module.exports = router;

function getAll(req, res) {
    auctionService.getAll()
        .then(function (auctions) {
            res.send(auctions);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getByLotName(req, res) {
    auctionService.getByLotName(req.params.name)
        .then(function (auctions) {
            res.send(auctions);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getByLotType(req, res) {
    auctionService.getByLotType(req.params.type)
        .then(function (auctions) {
            res.send(auctions);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getByLotName(req, res) {
    auctionService.getByLotName(req.params.name)
        .then(function (auctions) {
            res.send(auctions);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getById(req, res) {
    auctionService.getById(req.params.id)
        .then(function (auction) {
            if (auction) {
                res.send(auction);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function addBid(req, res) {
    auctionService.update(req.body.bid)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function createAuction(req, res) {
    auctionService.create(req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function update(req, res) {
    auctionService.update(req.params._id, req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function _delete(req, res) {
    auctionService.delete(req.params._id)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}