var config = require('config.json');
var express = require('express');
var router = express.Router();
var bidService = require('services/bid.service');

// routes
router.get('/:user_id', getByUserId);
router.get('/:auction_id', getByAuctionId);
router.get('/:_id', getById);
router.post('/create_auction', createBid)
router.delete('/:_id', _delete);

module.exports = router;

function getByUserId(req, res) {
    bidService.getByUserId(req.params.user_id)
        .then(function (bids) {
            res.send(bids);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getByAuctionId(req, res) {
    bidService.getByAuctionId(req.params.auction_id)
        .then(function (bids) {
            res.send(bids);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getById(req, res) {
    bidService.getById(req.params.id)
        .then(function (bid) {
            if (bid) {
                res.send(bid);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function createBid(req, res) {
    bidService.create(req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}


function _delete(req, res) {
    bidService.delete(req.params._id)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}