var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('auctions');

var service = {};

service.create = create;
service.update = update;
service.delete = _delete;

service.getAll = getAll;
service.getById = getById;
service.getByLotName = getByLotName;
service.getByLotType = getByLotType;

service.addBid = addBid


module.exports = service;

function getAll() {
    var deferred = Q.defer();

    db.auctions.find().toArray(function (err, auctions) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(auctions);
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.auctions.findById(_id, function (err, auction) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (auction) {
            
            deferred.resolve(auction);
        } else {
            // auction not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getByLotName(lotName) {
    var deferred = Q.defer();

    db.auctions.find(
        { lotName: lotName }
    ).toArray(function (err, auctions) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(bids);
    });

    return deferred.promise;
}

function getByLotType(lotType) {
    var deferred = Q.defer();

    db.auctions.find(
        { lotType: lotType }
    ).toArray(function (err, auctions) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(bids);
    });

    return deferred.promise;
}

function create(auction) {
    var deferred = Q.defer();
    
    db.auctions.insert(
            auction,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });

    return deferred.promise;
}

function update(_id, auctionParam) {
    var deferred = Q.defer();

    db.auctions.update(
        { _id: mongo.helper.toObjectID(_id) },
        { $set: auctionParam },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.auctions.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function addBid(bidParam) {
    var deferred = Q.defer();
    
    db.auctions.update(
            {_id: bidParam.id},
            { $push: { bids: bidParam.bidId } },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });

    return deferred.promise;
}