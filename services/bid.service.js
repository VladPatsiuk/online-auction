var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('bids');

var service = {};

service.create = create;
service.delete = _delete;

service.getAllBidsByUser = getAllBidsByUser;
service.getAllAuctionBids = getAllAuctionBids;
service.getById = getById;


module.exports = service;

function create(bid) {
    var deferred = Q.defer();
    
    db.bids.insert(
            bid,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.bids.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function getAllAuctionBids(auctionId) {
    var deferred = Q.defer();

    db.bids.find(
        {auctionId: auctionId}
        ).toArray(function (err, bids) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(bids);
    });

    return deferred.promise;
}

function getAllBidsByUser(userId) {
    var deferred = Q.defer();

    db.bids.find(
        {userId: userId}
        ).toArray(function (err, bids) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(bids);
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    db.bids.findById(_id, function (err, bid) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (bid) {
            
            deferred.resolve(auction);
        } else {
            deferred.resolve();
        }
    });

    return deferred.promise;
}

